Contemporary Project - Exultation
================================

![Exultation cover](https://i1.sndcdn.com/artworks-000446052531-1n05n7-t500x500.jpg)


```
Luca Pedeferri _ piano _ accordion
Fausto Tagliabue _ trumpet _ flugelhorn _ voice
Lionello (Lello) Colombo _ tenor sax
Marco Menaballi _ electronics _ turntables
Enrico Fagnoni _ double bass
Mauro Gnecchi _ drums _ percussions

01 _ Frisking
02 _ The Hero Sun
03 _ The Banshee
04 _ The Voice of Lir
05 _ Fabric
06 _ The Tides of Manaunaun
07 _ Sinister Resonance
08 _ Anger Dance
09 _ Aeolian Harp
10 _ Exultation
0A _ Exultation (4/4 Re-Mix)
```


Listen to [audio files on sound cloud](https://l.facebook.com/l.php?u=https%3A%2F%2Fsoundcloud.com%2Fluca-pedeferri%2Fsets%2Fexultation%3Ffbclid%3DIwAR2BokGha3ZaIIYT5SDbcTacPpYjMSNC9R3_lhgTFgEXk6N4-jfAEnqN4po&h=AT1cufgGlH4UY7QQ2xmjrAgSo3OgpXQpz4pmPoWIKdD5_kaym30Z65MUg9w3_PI82KPgJ4ep7rzlv7jjFyYsvGh_B1kPvm2MDNLjaylGO_jJtKOchRMSPFSJPsJSqHseM0T6Cg)

With this album continue the musical project of Luca Pedeferri with which he investigates the music of some composers of the XX Century. After the album dedicated to the Russian composer Galina Ustvol'skaja (see Meditation on Ustvol'skaja, SM3240) it is now the turn of Henry Cowell (1897-1965), a key musician of the American scene that, together with Charles Ives, can be defined one of the most important classical musicians of the Country, among the first to look for their own originality (he is the inventor of the 'cluster') and, if possible, from the cultural hegemony of the European musical 'diktat', so well received and pursued by the American conservatories - although we will have to wait John Cage and Morton Feldman for a truly evident break with the European's rules. This is an album that surprising for many reasons: excellent arrangements, excellent group and a varied language (mostly jazz oriented, contemporary music), always intelligible.
For more info: https://rebrand.ly/exultation

"(...) Continuing to pay tribute to the small but mighty Setola Di Maiale label headed by Stefano Giust on their 25th anniversary, I want to call attention to Exultation by pianist Luca Pedeferri and his group. The recording is a very original take on the music of Henry Cowell by a sextet comprised of traditional jazz quintet instrumentation along with electronics and turntables. Unusual and well thought-out, with a very cool remix of one of the tracks at the end." Craig Premo, Improvised, 2019.

"(...) De Italiaanse pianist en componist Luca Pedeferri houdt eraan zijn grote voorbeelden in de kijker te plaatsen. Nadat hij de focus richtte op de Russische Galina Ustvolskaya is nu de polyvalente en vooruitstrevende Amerikaanse componist en theoreticus Henry Cowell aan de beurt.
Voor dit eerbetoon selecteerde Pedeferri tien pianostukken uit Cowell’s prille beginperiode (1914-1930). Voor de uitvoering deed hij (opnieuw) beroep op de leden van zijn Contemporary Project: Lionello Colombo (tenorsaxofoon), Enrico Fagnoni (contrabas), Mauro Gneechi (drums, percussie), Marco Menaballi (electronics, turntables) en Fausto Tagliabue (trompet, flugelhorn).
Krachtige uitroeptekens van trompet en piano gelden als een eerste kennismaking. Tot hieruit plots een walsje ontstaat. Goochelende improvisators of improviserende klankgoochelaars? Het vervolg blijft even onvoorspelbaar over heel de lijn. ‘The Hero Sun’ begint met korte stroomstoten van een saxofoon en daarbij op de achtergrond een dreigend geluidsdecor. Er worden wat sciencefiction effecten aan toegevoegd en plots schalt er een heuse band door de luidsprekers met ritmesectie en blazers. Geen klassieke jazzband wel te verstaan. Eerder een gezelschap dat ook al eens luistert naar Mostly Other Peolple Do The Killing of FES. De broze outro krijg je er zomaar als toemaatje bij.
‘The Banshee’ begint als een soundtrack die bij de Japanse horrorfilm ‘Ringu’ (‘The Ring’) past. Huiveringwekkend zonder expliciete clichés, wel met Pedeferri op accordeon en trompettist Tagliabue die de meest gekke capriolen uithaalt. ‘Fabric’ is een heel poëtisch interludium met trompet en piano. ‘The Tides Of Manaunaum’ lijkt daarbij aan te sluiten maar al snel ontspoort alles en verdwaal je als luisteraar in de meest spannende decors, Fritz Lang’s ‘M - Eine Stadt Sucht Einen Mörder’ waardig. Humor, acoustics en electronics kruisen elkaar in ‘Anger Dance’.
Nummer na nummer geraak je verstrikt in een web van situaties waaruit geen ontkomen mogelijk lijkt. Decadentie en spanning worden genadeloos aan elkaar gekoppeld. Tonaal, atonaal en polyritmiek wisselen elkaar af in een ondoorgrondelijke volgorde. Pedeferri en zijn kompanen bieden uiteindelijk toch telkens een uitweg en zorgen voor een goede afloop. Hoe ze daartoe komen, blijft aanvankelijk een raadsel. Gelukkig is er de “repeat” knop om telkens te herbeginnen tot je hun werkwijze ontcijferd hebt. Henry Cowell (1897-1965) zou tevreden zijn met dergelijk eerbetoon. Zijn statement “I believe in music: its spirituality, its exaltation, its ecstatic nobility, its humor, its power to penetrate to the basic fineness of every human” wordt hier op passende wijze verklankt.
Een uiterst intrigerende kruisbestuiving tussen modernistisch klassiek, improvisatie en jazz. Absolute aanrader voor fans van Octurn en natuurlijk Charles Ives en Morton Feldman. De 4/4 RE-mix van de titeltrack kan zelfs uitgroeien tot een heuse dance hit." Georges Tonla Briquet, Jazz Halo, 2019.

"(...) Exultation è il nuovo progetto del pianista Luca Pedeferri, che si deve considerare la seconda incursione del musicista jazz nella musica contemporanea. Infatti, dopo l'interessamento per la musica di Galina Ustvolskaya, Pedeferri si getta a capofitto nelle tematiche essenziali del grande Henry Cowell; Exultation prevede un programma di rifacimento di 10 pezzi celebri del compositore americano, che informano sulla sua esperienza pianistica: come molti sapranno, Cowell è stato ufficialmente uno dei primi compositori a costruire nuove figurazioni armoniche al pianoforte attraverso tecniche pianistiche che all'epoca si potevano solo intuire. L'influenza di Cowell sulla musica moderna è stratosferica, specie quando si pensa a quanto messo in pratica sullo strumento in un'ottica di attualizzazione della storia: prestare attenzione alle corde interne dello strumento, impostare un secondo avventore del piano in funzione di aiuto, elaborare armonie in mezzo a serie definite di clusters o come coda dell'utilizzo della tastiera, passaggi concentrati sulle risonanze dei registri bassi della tastiera, sono tutte manovre a lui verginalmente riferibili. 
Come nel cd precedente dedicato alla Ustvolskaya, Pedeferri si circonda ancora di un ensemble di musicisti (il Contemporary Project comprende Fausto Tagliabue alla tromba e flicorno, Lionello Colombo al sax tenore, Marco Menaballi alla parte elettronica, Enrico Fagnoni al contrabbasso e Mauro Gnecchi alle percussioni) e sfodera un talento negli arrangiamenti che farebbe invidia a qualsiasi professionista del settore: si comprende come non solo Pedeferri abbia letteralmente ingoiato The piano music of H.C., ma anche come abbia previsto spazi di riadattamento che non coinvolgono solo il jazz, lasciando porte aperte anche per una libertà d'azione che non segue convenzioni. Da questo punto di vista, i dieci brani famosi di Cowell ricevono un trattamento che recepisce lo scheletro dell'impianto melodico e l'umore delle composizioni per piano di Cowell, ma si riprogetta con caratteristiche che non gli appartengono formalmente, dove gli strumenti aprono spazi di creatività non previsti dalle fasi pianistiche, si perviene a tagli e sostituzioni indotte da un diverso sentimento, ci si adopera per un tessuto essenzialmente jazzistico che comunque non scade mai in manierismi: per quelli che non conoscono Cowell, il mio consiglio è quello di incrociare ogni pezzo del suo Piano Music con quanto è stato riottenuto da Pedeferri, al fine di cogliere con più precisione le differenze a cui ho accennato, e comprendere il presunto trasbordo che Pedeferri vuole effettuare nella sua musica. Exultation non ha crepe, è impegnato e gradevole allo stesso tempo, e può essere una prima mappa da cui partire per prendere più coraggio su rapporti impensabili con la musica di Cowell, sfruttando le strutture compositive per intraprendere libere e spontanee improvvisazioni di musica non idiomatica." Ettore Garzia, Percorsi Musicali, 2018.

Gustavo Adolfo Bustamante: 
"(...) el sonido es un placer físico (o disgusto) que acompaña nuestras vidas casi sin parar, y la música, siendo un placer o un disgusto en sí misma, 
es solo una parte de un gran todo..." HOY, en 'Jazz Session' la obra "Exultation - A Musical Proyect" del italiano Luca Pedeferri. 
10 pm UN Radio 100.4 FM http://unradio.medellin.unal.edu.co